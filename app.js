// Module dependencies

var express = require('express');
var consolidate = require('consolidate');
var http = require('http');
var path = require('path');
var contracts = require('./routes/contracts');

var app = express();

// Config

app.set('port', process.env.PORT || 8402);
app.set('address', 'localhost');
app.set('core', 'http://localhost:8400');

app.engine('html', consolidate.handlebars);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.errorHandler());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Contracts

app.get('/', contracts.renderList);
app.get('/new', contracts.renderNew);
app.get('/:id', contracts.renderFound);

app.post('/', contracts.create);
app.post('/:id', contracts.update);
app.del('/:id', contracts.del);

http.createServer(app).listen(app.get('port'), app.get('address'), function() {
  console.log('Contract management listening on ' + app.get('address') + ':' + app.get('port'));
});
