var request = require('request');

exports.renderList = function(req, res) {
  request(req.app.get('core') + '/contracts', {headers:{accept:'application/json'}}, function (error, coreRes, body) {
    var contracts = JSON.parse(body);
    res.render('index', {title:'Contracts management', contracts:contracts, filter:contracts.length>1});
  });
};

exports.renderNew = function(req, res) {
  request(req.app.get('core') + '/contracts/status-types', {headers:{accept:'application/json'}}, function (error, coreRes, body) {
    var statusTypes = JSON.parse(body);

    res.render('contract', {title:'New contract', contract:{}, statusTypes:statusTypes, helpers:{selected:function(value, options) {
      return options.inverse(this);
    }}});
  });
};

exports.renderFound = function(req, res) {
  request(req.app.get('core') + '/contracts/status-types', {headers:{accept:'application/json'}}, function (error, coreRes, body) {
    var statusTypes = JSON.parse(body);

    request(req.app.get('core') + '/contracts/' + req.params.id, {headers:{accept:'application/json'}}, function (error, coreRes, body) {
      var contract = JSON.parse(body);

      res.render('contract', {title:'Contract', contract:contract, statusTypes:statusTypes, helpers:{selected:function(value, options) {
        return value && value === contract.statusType ? options.fn(this) : options.inverse(this);
      }}});
    });
  });
};

exports.create = function(req, res) {
  request.post(req.app.get('core') + '/contracts', {json: req.body}, function (error, coreRes, body) {
    res.send(201);
  });
};

exports.update = function(req, res) {
  request.post(req.app.get('core') + '/contracts/' + req.params.id, {json: req.body}, function (error, coreRes, body) {
    res.send(200);
  });
};

exports.del = function(req, res) {
  request.del(req.app.get('core') + '/contracts/' + req.params.id, function (error, coreRes, body) {
    res.send(200);
  });
};