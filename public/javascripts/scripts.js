$(document).on('pageinit', '#page-contract', function () {
  $('#button-contracts').on('tap', function () {
    $.mobile.changePage('/', {
      reverse: true,
      transition: 'slide',
      reloadPage: true
    });
  });

  $('#button-cancel').on('tap', function () {
    $.mobile.changePage('/', {
      reverse: true,
      transition: 'slideup',
      reloadPage: true
    });
  });

  $('#button-contract-save').on('tap', function () {
    var $form = $('#form-contract');

    $.mobile.loading('show');

    $.post($form.attr('action'), $form.serialize())
      .done(function () {
        $.mobile.loading("show", {
          text: 'Saved',
          theme: 'b',
          textVisible: true,
          textonly: true
        });
        if ($form.attr('action') === '/') {
          setTimeout(function () {
            $.mobile.changePage('/', {
              reverse: true,
              transition: 'slideup',
              reloadPage: true
            });
          }, 500);
        }
      })
      .fail(function () {
        $.mobile.loading("show", {
          text: 'Failed',
          theme: 'b',
          textVisible: true,
          textonly: true
        });
      })
      .always(function () {
        setTimeout(function () {
          $.mobile.loading("hide");
        }, 1000);
      });
  });

  $('#button-contract-delete').on('tap', function () {
    var $form = $('#form-contract');

    $.mobile.loading('show');

    $.ajax({url: $form.attr('action'), type: 'DELETE'})
      .done(function () {
        $.mobile.loading("show", {
          text: 'Deleted',
          theme: 'b',
          textVisible: true,
          textonly: true
        });
        setTimeout(function () {
          $.mobile.changePage('/', {
            reverse: true,
            transition: 'slide',
            reloadPage: true
          });
        }, 500);
      })
      .fail(function () {
        $.mobile.loading("show", {
          text: 'Failed',
          theme: 'b',
          textVisible: true,
          textonly: true
        });
      })
      .always(function () {
        setTimeout(function () {
          $.mobile.loading("hide");
        }, 1000);
      });
  });
});





